package NL.martijnpu.AntiFastJoin;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

class PlayerJoin implements Listener {
    private main plugin;

    PlayerJoin(main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerLoginEvent e) {
        if(plugin.playerJoinDisabled) {
            e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            e.setKickMessage("Joining too fast. Please wait a moment.");
        }
    }
}
package NL.martijnpu.AntiFastJoin;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


public class main extends  JavaPlugin{
    boolean playerJoinDisabled;

    @Override
    public void onEnable() {
        playerJoinDisabled = true;

        getServer().getPluginManager().registerEvents(new PlayerJoin(this), this);

        new BukkitRunnable() {
            @Override
            public void run() {
                playerJoinDisabled = false;
                System.out.print("[AntiFastJoin] Joining enabled again");
            }

        }.runTaskLater(this, 200);
    }
}

